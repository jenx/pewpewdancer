import pew

from level import TILES, generate


MOVEMENT = {
    pew.K_LEFT: (-1, 0),
    pew.K_RIGHT: (1, 0),
    pew.K_UP: (0, -1),
    pew.K_DOWN: (0, 1),
}

NO_SCROLL_ZONE = (2, 5)

x = NO_SCROLL_ZONE[0]
y = NO_SCROLL_ZONE[0]

level_x = 0
level_y = 0


def beat(level, screen, dx, dy):
    global x, y
    global level_x, level_y

    # Trying to go out of bounds, do not move
    if level_x + x + dx <= 0 or level_x + x + dx >= len(level[0]) - 1:
        return
    if level_y + y + dy <= 0 or level_y + y + dy >= len(level) - 1:
        return

    # Dig a wall
    if screen.pixel(x + dx, y + dy) == TILES['wall']:
        level[level_y + y + dy][level_x + x + dx] = TILES['empty']
        return

    # Move
    if NO_SCROLL_ZONE[0] <= x + dx <= NO_SCROLL_ZONE[1]:
        x += dx
    else:
        level_x += dx

    if NO_SCROLL_ZONE[0] <= y + dy <= NO_SCROLL_ZONE[1]:
        y += dy
    else:
        level_y += dy


def draw(screen, cycle, level):
    # Show map
    screen.blit(pew.Pix.from_iter(level), 0, 0, level_x, level_y, 8, 8)

    # Show character
    screen.pixel(x, y, cycle)

    # Advance
    pew.show(screen)
    pew.tick(1 / 12)


def play():
    global x, y
    global level_x, level_y

    level = generate(walliness=50)

    pew.init()
    screen = pew.Pix()

    cycle = 0
    last_active_keys = 0

    while True:
        # Move
        keys = pew.keys()
        new_keys = keys & ~last_active_keys
        for key, delta in MOVEMENT.items():
            if new_keys & key:
                beat(level, screen, *delta)
        last_active_keys = keys

        draw(screen, cycle, level)

        cycle = (cycle + 1) % 4


play()