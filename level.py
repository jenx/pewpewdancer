import random


WALL_CONTINUATION_PROBABILITY = 75
MIN_SPACING = 2

TILES = {
    'empty': 0,
    'wall': 1,
}


def coin_flip(target):
    return random.randint(1, 100) < target


def draw_vertical_wall(level, col, full=False):
    draw = full or coin_flip(WALL_CONTINUATION_PROBABILITY)
    for row in range(len(level)):
        if not full and level[row][col] == TILES['wall']:
            draw = coin_flip(WALL_CONTINUATION_PROBABILITY)
        if draw:
            level[row][col] = TILES['wall']


def draw_horizontal_wall(level, row, full=False):
    draw = full or coin_flip(WALL_CONTINUATION_PROBABILITY)
    for col in range(0, len(level[0])):
        if not full and level[row][col] == TILES['wall']:
            draw = coin_flip(WALL_CONTINUATION_PROBABILITY)
        if draw:
            level[row][col] = TILES['wall']


def generate_wall_positions(walliness, max_dim):
    walls = set()
    i = MIN_SPACING + 1
    while i < max_dim - MIN_SPACING - 1:
        if coin_flip(walliness):
            walls.add(i)
            i += MIN_SPACING + 1
            walliness -= 20
        else:
            i += 1
            walliness += 20
    return walls


def generate(walliness):
    # Generate empty level
    width = random.randint(10, 15)
    height = random.randint(10, 15)

    level = []
    for h in range(height):
        level.append([TILES['empty']] * width)

    # Generate walls
    horizontal_walls = generate_wall_positions(walliness, height)
    vertical_walls = generate_wall_positions(walliness, width)

    # Draw walls
    while horizontal_walls or vertical_walls:
        if coin_flip(50) and horizontal_walls:
            pos = horizontal_walls.pop()
            draw_horizontal_wall(level, pos)
        elif vertical_walls:
            pos = vertical_walls.pop()
            draw_vertical_wall(level, pos)

    draw_vertical_wall(level, 0, full=True)
    draw_vertical_wall(level, -1, full=True)
    draw_horizontal_wall(level, 0, full=True)
    draw_horizontal_wall(level, -1, full=True)

    return level